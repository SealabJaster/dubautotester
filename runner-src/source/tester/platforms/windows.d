module tester.platforms.windows;

private
{
    import std.conv : to;
    import tester.platforms.core, tester.platforms.util, tester.db;
}

final class TestPlatformWindows : ITestPlatform
{
    void onInit()
    {
    }

    void useComponent(Component comp, string versionID)
    {
        if(ComponentHelper.isCompiler(comp))
            ComponentHelper.useCompiler(comp, versionID);
        else
            Shell.echof("WARNING: Component %s version %s is not supported on this platform.", comp, versionID);
    }

    void useDefaultComponents()
    {
        ComponentHelper.useNLatestVersions(this, Component.Dmd, ComponentHelper.DMD_DEFAULT_VERSION_COUNT);
        ComponentHelper.useNLatestVersions(this, Component.Ldc, ComponentHelper.LDC_DEFAULT_VERSION_COUNT);
    }

    @property
    DbPlatform platform()
    {
        DbPlatform plat;
        if(!Db.doesExistByName!DbPlatform("windows"))
        {
            plat = DbPlatform(-1, "windows");
            Db.insert(plat);
        }
        else
            plat = Db.getStructByName!DbPlatform("windows");

        return plat;
    }
}