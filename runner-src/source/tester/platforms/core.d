module tester.platforms.core;

import tester.db : DbPlatform;

const VERSION_ANY = "*";

enum Component
{
    Dmd,
    Ldc
}

interface ITestPlatform
{
    void onInit();
    void useComponent(Component compiler, string versionID);
    void useDefaultComponents();

    @property
    DbPlatform platform();
}

ITestPlatform getPlatform()
{
    version(Windows)
    {
        import tester.platforms.windows;
        return new TestPlatformWindows();
    }
    else static assert(false, "Not supported.");
}