module tester.platforms.util;

private
{
    import core.time, std.datetime;
    import tester.platforms.util, tester.platforms.core;
}

final static class Shell
{
    static const RUN_TIMEOUT = 1.minutes;

    struct RunResult
    {
        string command;
        int    status;
        string output;
    }

    private static
    {
        string[] _locationStack;
    }

    public static
    {
        bool echo;

        RunResult run(string command)
        {
            import std.stdio     : File;
            import std.process   : pipeShell, tryWait, kill, Redirect, ProcessPipes;
            import std.exception : assumeUnique;
            import vibe.d        : ManualEvent, runWorkerTask;

            Shell.echof("run: %s", command);

            auto pipes = pipeShell(command, Redirect.stdout | Redirect.stderrToStdout);
            char[] output;
            int status;
            shared ManualEvent done;

            // Why oh why must reading from stdout be blocking....
            // Why oh why must Vibe-d be so good at it's job for making it hard for me to do this....
            // TODO: Find out if there's any easy, cross-platform way in Phobos to make this non-blocking T.T
            // Also TODO: Find a nice way around multiple tasks wanting to use `pushLocation` in a way that doesn't make
            //            every other task break, since previously the blocking operation in this function would essentially
            //            force them to follow the right order.
            static void runner(shared size_t pipes_, shared size_t output_, shared size_t status_, shared size_t done_) 
            {
                auto pipes  = cast(ProcessPipes*)pipes_;
                auto output = cast(char[]*)output_;
                auto status = cast(int*)status_;
                auto done   = cast(shared ManualEvent*)done_;
                while(true)
                {
                    auto tuple = tryWait(pipes.pid);
                    foreach(chunk; pipes.stdout.byChunk(4096))
                        *output ~= chunk;

                    if(tuple.terminated)
                    {
                        *status = tuple.status;
                        done.emit();
                        break;
                    }
                }
            }

            // This is safe. The stack stays alive for the entire lifetime of the task, and the values referenced are never written to
            // outside of the task, and are never read from until after the task is finished.
            // Exception is `done.emitCount` which is already thread safe. Or if Task.terminate/interrupt/whatever_it_was is used... God bless
            runWorkerTask(&runner, cast(size_t)&pipes, cast(size_t)&output, cast(size_t)&status, cast(size_t)&done);

            while(true)
            {
                if(done.emitCount > 0)
                    break;

                import vibe.core.core : vibeYield = yield;
                vibeYield();
            }

            //Shell.echof(output.assumeUnique);

            return RunResult(command, status, output.assumeUnique);
        }

        RunResult run(string command, string[] args)
        {
            import std.algorithm : joiner;
            import std.array     : array;
            import std.conv      : to;
            return Shell.run(command ~ " " ~ args.joiner(" ").array.to!string);
        }

        RunResult runEnforceStatusPositive(string command, string[] args = null)
        {
            import std.exception : enforce;
            
            auto result = Shell.run(command, args);
            enforce(result.status >= 0, new ShellException(result));
            
            return result;
        }

        RunResult runEnforceStatusZero(string command, string[] args = null)
        {
            import std.exception : enforce;
            
            auto result = Shell.run(command, args);
            enforce(result.status == 0, new ShellException(result));
            
            return result;
        }

        void echof(Args...)(string fmt, Args args)
        {
            import vibe.d : logInfo;
            if(Shell.echo)
                logInfo(fmt, args);
        }
        
        bool doesCommandExist(string command)
        {
            version(Windows)
            {
                // The '--help' and '--version' are just to lower the possibility of actually running the
                // command by accident.
                return Shell.run(command, ["--help", "--version"]).status != 9009; // 9009 = Command not found.
            }
            else
                static assert(false, "Not supported");
        }

        string pushLocation(string location)
        {
            import std.file : chdir, getcwd;

            Shell.echof("pushLoc: %s", location);

            this._locationStack ~= getcwd();
            chdir(location);

            return getcwd();
        }

        string popLocation()
        {
            import std.file      : chdir;
            import std.exception : enforce;

            enforce(this._locationStack.length > 0, "Cannot pop an empty stack. Uneven amount of calls to popLocation compared to pushLocation.");

            auto loc = this._locationStack[$-1];
            this._locationStack.length -= 1;

            Shell.echof("popLoc: %s", loc);

            chdir(loc);
            return loc;
        }

        void ensureDirectoryExists(string directory)
        {
            import std.file : exists, mkdirRecurse;
            if(!directory.exists)
            {
                Shell.echof("mkdirRecurse: %s", directory);
                mkdirRecurse(directory);
            }
        }
    }
}

final static class Git
{
    public static
    {
        void enforceExists()
        {
            import std.exception : enforce;
            enforce(Shell.doesCommandExist("git"), "'git' is either not installed, or not on the PATH.");
        }

        GitRepo cloneOrOpen(string url, string location = "repo_cache")
        {
            import std.path : buildNormalizedPath;
            import std.file : exists;

            Shell.ensureDirectoryExists(location);

            // Get the last part of the URL
            string lastPart;
            bool endsWithSlash;
            for(size_t i = url.length; i > 0; i--)
            {
                auto next = url[i - 1];
                if(i == url.length && next == '/')
                {
                    endsWithSlash = true;
                    continue;
                }

                if(next == '/')
                {
                    auto end = (endsWithSlash) ? url.length - 1 : url.length;
                    lastPart = url[i..end];
                    break;
                }
            }

            // Check if it exists already, then open/clone it.
            auto cwd = Shell.pushLocation(location);
            scope(exit) Shell.popLocation();

            auto repoPath = buildNormalizedPath(cwd, lastPart);
            if(!repoPath.exists)
                Shell.runEnforceStatusZero("git clone", [url]);

            return new GitRepo(repoPath);
        }
    }
}

final class GitRepo
{
    private
    {
        string _location;
    }

    this(string location)
    {
        this._location = location;
    }

    public
    {
        void changeTag(string tag)
        {
            Shell.pushLocation(this.location);
            scope(exit) Shell.popLocation();

            Shell.runEnforceStatusZero("git checkout tags/"~tag);
        }

        @property
        string[] tags()
        {
            import std.algorithm : splitter, filter;
            import std.array     : array;

            Shell.pushLocation(this.location);
            scope(exit) Shell.popLocation();

            Shell.runEnforceStatusZero("git fetch --tags");
            auto result = Shell.runEnforceStatusZero("git tag");
            return result.output.splitter("\n").filter!(s => s.length > 0).array;
        }

        @property
        string location()
        {
            return this._location;
        }
    }
}

final static class Zip
{
    public static
    {
        void enforceExists()
        {
            import std.exception : enforce;
            enforce(Shell.doesCommandExist("7za"), "'7za' (7-zip) is either not installed, or not on the PATH.");
        }

        void unzip(string zipLocation, string destination)
        {
            import std.exception : enforce;
            import std.file      : exists, mkdirRecurse;

            Shell.echof("unzip: FROM(%s)\n"
                       ~"       TO(%s)", zipLocation, destination);

            enforce(zipLocation.exists, "The zip file doesn't exist.");
            
            Shell.ensureDirectoryExists(destination);
            Shell.runEnforceStatusZero("7za x", [zipLocation, "-o"~destination, "-y"]); // Can't have spaces in 7-zip arguments
        }

        void downloadAndUnzip(string url, string destination)
        {
            import vibe.inet.urltransfer : download;
            import std.file              : remove;

            Shell.echof("download: %s", url);

            const TEMP_FILE = "downloaded.temp";
            download(url, TEMP_FILE);
            scope(exit) remove(TEMP_FILE);

            Zip.unzip(TEMP_FILE, destination);
        }
    }
}

final static class ComponentHelper
{
    import tester.db;

    public static
    {
        const DMD_DOWNLOAD_FORMAT_STRING = `http://downloads.dlang.org/releases/2.x/%s/dmd.%s.windows.7z`;
        const DMD_MAIN_REPO              = `https://github.com/dlang/dmd`;
        const DMD_DEFAULT_VERSION_COUNT  = 3;
        const LDC_DOWNLOAD_FORMAT_STRING = `https://github.com/ldc-developers/ldc/releases/download/v%s/ldc2-%s-windows-x64.7z`;
        const LDC_MAIN_REPO              = `https://github.com/ldc-developers/ldc`;
        const LDC_DEFAULT_VERSION_COUNT  = 3;
        const VERSION_REGEX              = `([0-9]+\.[0-9]+\.[0-9]+)`;
        const VERSION_MINOR_REGEX        = `[0-9]+\.([0-9]+)\.[0-9]+`;
        const COMPILER_CACHE             = "compilers";

        void useCompiler(Component comp, string versionID)
        {
            import std.file      : exists;
            import std.path      : buildPath;
            import std.format    : format;
            import std.exception : enforce;
            import std.conv      : to;

            enforce(ComponentHelper.isCompiler(comp), "Invalid component. Expected a compiler, not a %s".format(comp));
            if(versionID == VERSION_ANY)
                versionID = ComponentHelper.getRepo(comp).tags[$-1];

            Shell.echof("Using - Compiler:%s | Version:%s", comp, versionID);

            string platformString;
            version(Windows) platformString = "windows";
            else static assert(false, "Need support here for this platform");

            auto location = ComponentHelper.getCachedLocation(comp, versionID);
            if(!location.exists)
            {
                Zip.downloadAndUnzip(ComponentHelper.getDownloadURL(comp, versionID), location);

                DbCompiler info;
                info.id       = -1;
                info.name     = comp.to!string;
                info.version_ = versionID;
                foreach(dbComp; Db.getAll!DbCompiler())
                {
                    if(dbComp.name == info.name && dbComp.version_ == info.version_)
                    {
                        info = dbComp;
                        break;
                    }
                }

                if(comp == Component.Dmd)
                    info.path = buildPath(location, "dmd2", platformString, "bin", "dmd");
                else if(comp == Component.Ldc)
                    info.path = buildPath(location, format("ldc2-%s-%s-x64", versionID[1..$], platformString), "bin", "ldc2"); // [1..$] to strip off the starting 'v'
                else
                    assert(false, "Unsupported compiler");

                (info.id == -1) ? Db.insert(info) : Db.insertOrUpdate(info);
            }
        }

        void useNLatestVersions(ITestPlatform platform, Component comp, size_t n)
        {
            import std.regex     : regex, matchFirst;
            import std.algorithm : reverse, canFind;
            import std.conv      : to;
            import std.exception : enforce;

            enforce(ComponentHelper.isCompiler(comp));

            auto reg         = regex(VERSION_MINOR_REGEX);
            uint largerMinor = uint.max;
            uint usedCount   = 0;
            auto repo        = ComponentHelper.getRepo(comp);

            enforce(repo !is null, "No repo for component");

            foreach(tag; repo.tags.reverse)
            {
                if(tag.canFind("-")) // Skips "-rc", "-beta" etc.
                    continue;

                auto minor = matchFirst(tag, reg)[1].to!uint;
                if(minor < largerMinor)
                {
                    largerMinor = minor;
                    usedCount++;
                    platform.useComponent(comp, tag);

                    if(usedCount >= n)
                        break;
                }
            }
        }

        GitRepo getRepo(Component comp)
        {
            switch(comp) with(Component)
            {
                case Dmd: return Git.cloneOrOpen(DMD_MAIN_REPO);
                case Ldc: return Git.cloneOrOpen(LDC_MAIN_REPO);
                default:  return null;
            }
        }

        string getDownloadURL(Component comp, string versionID)
        {
            import std.regex  : regex, matchFirst;
            import std.format : format;

            switch(comp) with(Component)
            {
                case Dmd:
                    auto reg = regex(VERSION_REGEX);
                    auto ver = matchFirst(versionID, reg)[1];
                    return format(DMD_DOWNLOAD_FORMAT_STRING, ver, ver);

                case Ldc:
                    auto reg = regex(VERSION_REGEX);
                    auto ver = matchFirst(versionID, reg)[1];
                    return format(LDC_DOWNLOAD_FORMAT_STRING, ver, ver);
                
                default:
                    throw new Exception("No download link for %s yet.".format(comp));
            }
        }

        string getCachedLocation(Component comp, string versionID)
        {
            import std.path : buildNormalizedPath;
            import std.file : getcwd;

            switch(comp) with(Component)
            {
                case Dmd:
                    return buildNormalizedPath(getcwd(), COMPILER_CACHE, "dmd", versionID);

                case Ldc:
                    return buildNormalizedPath(getcwd(), COMPILER_CACHE, "ldc", versionID);
                
                default:
                    throw new Exception("No cached location for %s yet.".format(comp));
            }
        }

        bool isCompiler(Component comp)
        {
            return (comp == Component.Dmd || comp == Component.Ldc);
        }
    }
}

class ShellException : Exception
{
    Shell.RunResult result;

    this(Shell.RunResult result, string file = __FILE__, size_t line = __LINE__)
    {
        import std.format : format;

        this.result = result;
        super(
            format("Exit code: %s\n Command: %s\nCommand output:%s\n", 
                   result.status, result.command, result.output),
            file,
            line
        );
    }
}