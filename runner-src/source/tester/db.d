module tester.db;

private
{
    import d2sqlite3;
    import tester.platforms.util;

    const SETUP_DATABASE = import("sql/setup_database.sql");
}

public import std.datetime : SysTime, Clock;

struct TableName
{
    string name;
}

@TableName("project")
struct DbProject
{
    int id;
    string name;
    string repoKind;
    string repoUser;
    string repoProject;

    string getRepoUrl()
    {
        import std.format : format;

        return format("https://www.%s.com/%s/%s", this.repoKind, this.repoUser, this.repoProject);
    }
}

@TableName("project_version")
struct DbProjectVersion
{
    int id;
    string name;
    int projectId;
    SysTime dateTime;
}

@TableName("project_info")
struct DbProjectInfo
{
    import vibe.d : Json;

    int id;
    string name;
    string json;
    SysTime dateFetched;

    Json getInfo()
    {
        import vibe.d : parseJsonString;
        return parseJsonString(this.json);
    }
}

@TableName("architecture")
struct DbArchitecture
{
    int id;
    string name;
}

@TableName("platform")
struct DbPlatform
{
    int id;
    string name;
}

@TableName("compiler")
struct DbCompiler
{
    int id;
    string name;
    string version_;
    string path;
}

enum SuccessFlags
{
    None  = 0,
    Build = 1 << 0,
    Test  = 1 << 1,
    Fetch = 1 << 2
}

@TableName("test_result")
struct DbTestResult
{
    int id;
    int projectId;
    int projectVersionId;
    int architectureId;
    int compilerId;
    int platformId;
    SuccessFlags successFlags;
    string fetchLog;
    string buildLog;
    string testLog;
}

@TableName("bookeeping")
struct DbBookkeeping
{
    int id;
    SysTime dateLastFetched;
    string  codeDlangIndex;
    SysTime dateIndexLastFetched;
}

public static class Db
{
    static const DATABASE_FILE = ":memory:";

    struct StringArrayStruct
    {
        alias arr this;
        string[] arr;
    }
    alias Blacklist = StringArrayStruct;
    alias Whitelist = StringArrayStruct;

    private static
    {
        Database _db;
        bool     _inTransaction;
    }

    public static
    {
        void onInit(string dbFile = DATABASE_FILE)
        {
            import std.file : exists;

            bool doSetup = false;
            if(dbFile == ":memory:" || !dbFile.exists)
                doSetup = true;

            this._db = Database(dbFile);

            if(doSetup)
            {
                auto book = DbBookkeeping(1);
                book.dateLastFetched = SysTime(0);
                book.dateIndexLastFetched = SysTime(0);
                book.codeDlangIndex = "[]";

                this._db.run(SETUP_DATABASE);
                Db.insertOrUpdate(book);
            }
        }

        bool doesExistByName(T)(string name)
        {
            static assert(__traits(hasMember, T, "name"));

            auto statement = this._db.prepare("SELECT * FROM "~getTableNameFor!T~" WHERE NAME = :name");
            statement.bind(":name", name);
            
            return !statement.execute().empty;
        }

        bool doesExistByID(T)(int id)
        {
            static assert(__traits(hasMember, T, "id"));

            auto statement = this._db.prepare("SELECT * FROM "~getTableNameFor!T~" WHERE ID = :id");
            statement.bind(":id", id);
            
            return !statement.execute().empty;
        }

        bool doesTestResultExist(DbProject project, DbProjectVersion version_, DbArchitecture arch, DbCompiler compiler)
        {
            auto statement = this._db.prepare(
                "SELECT * FROM test_result "
               ~"WHERE PROJECT_ID         = :project_id "
               ~"AND   PROJECT_VERSION_ID = :version_id "
               ~"AND   ARCHITECTURE_ID    = :arch_id "
               ~"AND   COMPILER_ID        = :compiler_id;"
            );

            statement.bind(":project_id",  project.id);
            statement.bind(":version_id",  version_.id);
            statement.bind(":arch_id",     arch.id);
            statement.bind(":compiler_id", compiler.id);
            
            return !statement.execute().empty;
        }

        void doTransaction(Func)(Func func)
        {
            // Wait for any existing transaction to end.
            import vibe.core.core : vibeYield = yield;
            while(this._inTransaction)
                vibeYield();

            // Set the transaction flag so we don't do it multiple times.
            this._inTransaction = true;
            scope(exit) this._inTransaction = false;

            // Perform the transaction.
            this._db.begin();
            scope(failure) this._db.rollback();
            scope(success) this._db.commit();

            func();
        }

        string getTableNameFor(T)()
        {
            import std.traits : getUDAs;
            return getUDAs!(T, TableName)[0].name;
        }

        auto getAll(T)()
        if(is(T == struct))
        {
            import std.algorithm : map;
            return this._db.execute("SELECT * FROM "~getTableNameFor!T~";").map!(r => Db.rowToStruct!T(r));
        }

        T[] getAllEager(T)()
        {
            import std.array : array;
            return Db.getAll!T().array;
        }

        T getStructById(T)(int id)
        if(is(T == struct))
        {
            import std.format : format;

            // If you can do some kind of SQL attack from an int, i'd be impressed
            auto result = this._db.execute("SELECT * FROM %s WHERE ID = %s;".format(Db.getTableNameFor!T, id));
            auto row    = result.front;

            return Db.rowToStruct!T(row);
        }

        T getStructByName(T)(string name)
        if(is(T == struct))
        {
            import std.format : format;

            auto statement = this._db.prepare("SELECT * FROM %s WHERE NAME = :name;".format(Db.getTableNameFor!T));
            statement.bind(":name", name);
            
            auto result = statement.execute();
            auto row    = result.front;

            return Db.rowToStruct!T(row);
        }

        auto getAllVersionsFor(DbProject project)
        {
            import std.algorithm : map;
            import std.format    : format;

            auto statement = this._db.prepare("SELECT * FROM %s WHERE PROJECT_ID = :id;".format(Db.getTableNameFor!DbProjectVersion));
            statement.bind(":id", project.id);

            return statement.execute.map!(Db.rowToStruct!DbProjectVersion);
        }

        string fieldToColumnName(string fieldName)
        {
            import std.ascii     : toUpper, isUpper;
            import std.conv      : to;
            import std.exception : assumeUnique;
            char[] buffer;

            foreach(ch; fieldName)
            {
                auto chr = ch.to!char;

                if(ch.isUpper)
                    buffer ~= '_';

                buffer ~= chr.toUpper;
            }

            // In case we have to do something like "version_" on the D side.
            if(buffer[$-1] == '_')
                buffer.length -= 1;

            return buffer.assumeUnique;
        }

        void bindStructToStatement(T)(T value, Statement statement, Blacklist blacklist = Blacklist.init)
        {   
            import std.algorithm : canFind;
            import std.traits    : FieldNameTuple, FieldTypeTuple, OriginalType;

            alias Names = FieldNameTuple!T;
            alias Types = FieldTypeTuple!T;

            auto getter(string name)() { mixin("return value."~name~";"); }

            static foreach(i, name; Names)
            {{
                if(!blacklist.canFind(name))
                {
                    alias Type     = Types[i];
                    const bindName = ":"~name;

                    static if(is(Type == enum))
                        statement.bind(bindName, cast(OriginalType!Type)getter!name());
                    else static if(is(Type == SysTime))
                        statement.bind(bindName, getter!name().toISOString());
                    else
                        statement.bind(bindName, getter!name());
                }
            }}
        }

        void insertOrUpdate(T)(T value, Blacklist blacklist = Blacklist.init)
        if(is(T == struct))
        {
            import std.traits    : FieldNameTuple;
            import std.format    : format;
            import std.algorithm : joiner;

            const tableName = Db.getTableNameFor!T;
            string[] columnNames;
            string[] fieldNames;

            // projectVersionId -> PROJECT_VERSION_ID
            static foreach(name; FieldNameTuple!T)
            {{
                columnNames ~= Db.fieldToColumnName(name);
                fieldNames  ~= ":"~name;
            }}

            auto sql = "INSERT OR REPLACE INTO %s (%s) VALUES (%s);".format(
                tableName,
                columnNames.joiner(", "),
                fieldNames.joiner(", ")
            );
            auto statement = this._db.prepare(sql);
            Db.bindStructToStatement(value, statement, blacklist);

            statement.execute();
        }

        void insert(T)(ref T value)
        {
            Db.insertOrUpdate(value, Blacklist(["id"]));
            value.id = this._db.execute("SELECT last_insert_rowid();").oneValue!int;
        }

        deprecated("Use Db.insert instead of Db.insertOrUpdate")
        int getNextIDFor(T)()
        {
            import std.traits : getUDAs;
            enum UDA = getUDAs!(T, TableName)[0];
            const tableName = UDA.name;

            auto result = this._db.execute("SELECT seq FROM sqlite_sequence WHERE name = '"~tableName~"' LIMIT 1;");
            auto id     = (result.empty) ? 1 : result.oneValue!int;
            
            while(Db.doesExistByID!T(id))
                id++;

            return id;
        }

        alias CompilePls = insertOrUpdate!DbTestResult;

        T rowToStruct(T)(Row row)
        if(is(T == struct))
        {
            import std.traits : FieldTypeTuple, FieldNameTuple, OriginalType;

            alias Names = FieldNameTuple!T;
            alias Types = FieldTypeTuple!T;

            T value;
            void setter(string CompilePls, V)(V v) { mixin("value."~CompilePls~" = v;"); }

            static foreach(i, name; Names)
            {{
                alias Type = Types[i];

                static if(is(Type == enum))
                    setter!(name, Type)(cast(Type)row.peek!(OriginalType!Type)(i));
                else static if(is(Type == SysTime))
                    setter!(name, Type)(SysTime.fromISOString(row.peek!string(i)));
                else
                    setter!(name, Type)(row.peek!Type(i));
            }}

            return value;
        }

        /// USE ONLY FOR SPECIAL CIRCUMSTANCES. MAKE A FUNCTION OTHERWISE.
        @property
        Database db()
        {
            return this._db;
        }
    }
}