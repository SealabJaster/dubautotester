module tester.runner;

private
{
    import vibe.d;
    import std.exception : enforce;
    import tester.platforms.core, tester.platforms.util, tester.db;
}

struct ScheduledTest
{
    DbProject        project;
    DbProjectVersion version_;
    DbArchitecture   arch;
    DbCompiler       compiler;
}

final static class TestRunner
{
    static const PACKAGE_LOCATION   = "package_cache";
    static const NOT_RAN_LOG        = "Step not ran since a previous step failed.";
    static const RUNS_BEFORE_PURGE  = 2700; // 3 archs for 3 versions for 3 compilers for 100 items

    private static
    {
        ITestPlatform   _platform;
        ScheduledTest[] _tests;
    }

    public static
    {
        void addToSchedule(ScheduledTest test)
        {
            import std.algorithm : canFind;
            if(this._tests.canFind(test))
                return;
            
            this._tests ~= test;
        }

        void onInit(ITestPlatform platform, string name)
        {
            import std.file : exists, mkdirRecurse;

            enforce(platform !is null);

            if(!PACKAGE_LOCATION.exists)
                PACKAGE_LOCATION.mkdirRecurse();

            Shell.echo = true;
            Git.enforceExists();
            Zip.enforceExists();
            enforce(Shell.doesCommandExist("dub"), 
                "Because of laziness, dub needs to be preinstalled. "
               ~"Eventually I'll bother to learn it's code properly so this won't be needed, since it's already used as a library."
            );

            this._platform = platform;
            platform.onInit();
            platform.useDefaultComponents();

            runTask(() => scheduleTask());
        }

        void purgeDownloads()
        {
            import std.algorithm : splitter, filter;

            bool first = false;
            foreach(name; Shell.run("dub list").output.splitter('\n'))
            {
                if(!first)
                {
                    first = true;
                    continue;
                }

                auto split = name.splitter(' ');
                while(!split.empty && split.front.length == 0) 
                    split.popFront();
                if(split.empty)
                    continue;

                Shell.run("dub remove", [split.front, "--version", "*"]);
            }

            import std.file : rmdir, mkdirRecurse;
            try rmdir(PACKAGE_LOCATION); catch(Exception ex){}
            mkdirRecurse(PACKAGE_LOCATION);
        }

        DbTestResult runTest(ScheduledTest test)
        {
            if(Db.doesTestResultExist(test.project, test.version_, test.arch, test.compiler))
                return DbTestResult.init;
            
            logInfo("Running test for '%s %s' with '%s %s%s'",
                    test.project.name, test.version_.name, test.arch.name, test.compiler.name, test.compiler.version_);

            DbTestResult result;
            result.projectId        = test.project.id;
            result.projectVersionId = test.version_.id;
            result.architectureId   = test.arch.id;
            result.compilerId       = test.compiler.id;
            result.platformId       = this._platform.platform.id;

            auto dubAddRemoveArgs = [test.project.name, test.version_.name];
            auto dubBuildTestArgs = ["-a", test.arch.name, "--compiler", `"` ~ test.compiler.path ~ `"`];

            scope(exit) Shell.run("dub remove-override", dubAddRemoveArgs);

            // TODO: A bit of DRY is needed here.
            // Fetch.
            GitRepo repo;
            try
            {
                repo = Git.cloneOrOpen(test.project.getRepoUrl(), PACKAGE_LOCATION);
                repo.changeTag("v"~test.version_.name);

                result.fetchLog = Shell.runEnforceStatusZero("dub add-override", dubAddRemoveArgs ~ repo.location).output;
                result.successFlags |= SuccessFlags.Fetch;
            }
            catch(ShellException ex)
            {
                result.fetchLog = ex.msg;
                result.buildLog = NOT_RAN_LOG;
                result.testLog  = NOT_RAN_LOG;
                return result;
            }

            Shell.pushLocation(repo.location);
            scope(exit) Shell.popLocation();

            // Build.
            try
            {
                auto runResult = Shell.runEnforceStatusZero("dub build", dubBuildTestArgs);
                result.buildLog = runResult.command ~ "\n" ~ runResult.output;
                result.successFlags |= SuccessFlags.Build;
            }
            catch(ShellException ex)
            {
                result.buildLog = ex.msg;
                result.testLog  = NOT_RAN_LOG;
                return result;
            }

            // Test
            try
            {
                auto runResult = Shell.runEnforceStatusZero("dub test", dubBuildTestArgs);
                result.testLog = runResult.command ~ "\n" ~ runResult.output;
                result.successFlags |= SuccessFlags.Test;
            }
            catch(ShellException ex)
            {
                result.testLog = ex.msg;
                return result;
            }

            return result;
        }
    }

    private static
    {
        void scheduleTask()
        {
            int runCount = 0;
            while(true)
            {
                try
                {
                    while(this._tests.length > 0)
                    {
                        auto result = TestRunner.runTest(this._tests[0]);
                        Db.insert(result);

                        if(this._tests.length == 1)
                            this._tests.length = 0;
                        else
                            this._tests = this._tests[1..$];

                        // Since this will be ran on a rather limited VM, I want to purge the downloaded stuff after a large amount of runs.
                        runCount++;
                        if(runCount >= RUNS_BEFORE_PURGE)
                        {
                            TestRunner.purgeDownloads();
                            runCount = 0;
                        }
                    }
                }
                catch(Exception ex)
                    logError("Runner task caught exception: %s", ex.msg);

                vibe.core.core.yield();
            }
        }
    }
}