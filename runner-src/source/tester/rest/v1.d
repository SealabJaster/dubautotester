module tester.rest.v1;

private
{
    import std.typecons : Nullable, nullable;
    import vibe.d;
    import tester.db;
}

struct RestCompiler
{
    string name;
    string version_;
}

struct RestTestResult
{
    string       project;
    string       version_;
    string       arch;
    string       platform;
    RestCompiler compiler;
    bool         passedFetch;
    bool         passedBuild;
    bool         passedTest;
    string       logFetch;
    string       logBuild;
    string       logTest;
}

@path("/api/v1/")
interface IRestInterfaceV1
{
    struct ProjectVersionInfo
    {
        string   project;
        string[] versions;
    }

    RestCompiler[]       getCompilers();
    string[]             getArchs();
    string[]             getProjects();
    string[]             getPlatforms();
    ProjectVersionInfo   getProjectVersions(string name);
    RestTestResult[]     getTestResults(string          name, 
                                        Nullable!int    version_,
                                        Nullable!string arch,
                                        Nullable!string compiler,
                                        Nullable!string platform,
                                        Nullable!bool   ifFetched,
                                        Nullable!bool   ifBuilt,
                                        Nullable!bool   ifTested,
                                        Nullable!bool   omitLog
                                       );
}

final class RestInterfaceV1 : IRestInterfaceV1
{
    import std.array     : array;
    import std.algorithm : map;
    import std.exception : enforce;
    import std.format    : format;

    RestCompiler[] getCompilers()
    {
        return Db.getAll!DbCompiler().map!(c => RestCompiler(c.name, c.version_)).array;
    }

    string[] getArchs()
    {
        return Db.getAll!DbArchitecture().map!(a => a.name).array;
    }

    string[] getProjects()
    {
        return Db.getAll!DbProject().map!(p => p.name).array;
    }

    string[] getPlatforms()
    {
        return Db.getAll!DbPlatform().map!(p => p.name).array;
    }

    ProjectVersionInfo getProjectVersions(string name)
    {
        enforceHTTP(
            Db.doesExistByName!DbProject(name),
            HTTPStatus.notFound,
            format("No project called '%s' exists.", name)
        );

        auto project = Db.getStructByName!DbProject(name);
        return ProjectVersionInfo(
                    project.name,
                    Db.getAllVersionsFor(project)
                      .map!(v => v.name)
                      .array
                );
    }

    // May whatever god you believe in decide to save my sinful soul.
    RestTestResult[]     getTestResults(string          name, 
                                        Nullable!int    version_,
                                        Nullable!string arch,
                                        Nullable!string compiler,
                                        Nullable!string platform,
                                        Nullable!bool   ifFetched,
                                        Nullable!bool   ifBuilt,
                                        Nullable!bool   ifTested,
                                        Nullable!bool   omitLog
                                       )
    {
        import d2sqlite3;

        alias BindFunc = void delegate(string name);

        string[]   columnNames;
        string[]   bindNames;
        Statement  statement;
        BindFunc[] binders;
        string[]   flagSelectors;

        void bind(T)(string name, T value)
        {
            columnNames ~= Db.fieldToColumnName(name);
            bindNames   ~= ":"~name;
            binders     ~= (n){ statement.bind(n, value); };
        }

        S getFromDb(S, K)(K key)
        {
            static if(is(K == int))
            {
                alias Checker = Db.doesExistByID!S;
                alias Getter  = Db.getStructById!S;
            }
            else
            {
                alias Checker = Db.doesExistByName!S;
                alias Getter  = Db.getStructByName!S;
            }

            import std.format : format;
            enforceHTTP(
                Checker(key),
                HTTPStatus.notFound,
                format("Type: %s | Key: '%s'", S.stringof, key)
            );

            return Getter(key);
        }

        void bindFromDbId(S, K)(string name, K key)
        {
            bind(name, getFromDb!S(key).id);
        }

        void bindFromDbIdNullable(S, K)(string name, Nullable!K key)
        {
            if(!key.isNull)
                bindFromDbId!S(name, key.get);
        }

        void applyMask(Nullable!bool flag, SuccessFlags value)
        {
            if(!flag.isNull)
                flagSelectors ~= format("(SUCCESS_FLAGS & %s) > 0", cast(int)value);
        }

        bindFromDbId!DbProject("projectId", name);
        bindFromDbIdNullable!DbProjectVersion("projectVersionId", version_);
        bindFromDbIdNullable!DbArchitecture("architectureId", arch);
        bindFromDbIdNullable!DbCompiler("compilerId", compiler);
        bindFromDbIdNullable!DbPlatform("platformId", platform);

        applyMask(ifFetched, SuccessFlags.Fetch);
        applyMask(ifBuilt, SuccessFlags.Build);
        applyMask(ifTested, SuccessFlags.Test);

        import std.format    : format;
        import std.algorithm : map, joiner;
        import std.range     : iota, chain;
        import std.array     : array;
        statement = Db.db.prepare("SELECT * FROM test_result WHERE %s;".format(
            iota(0, bindNames.length)
                .map!(i => format("%s = %s", columnNames[i], bindNames[i]))
                .chain(flagSelectors)
                .joiner(" AND ")
        ));

        foreach(i, binder; binders)
            binder(bindNames[i]);

        bool includeLog = (omitLog.isNull || omitLog.get) ? false : true;
        return statement.execute().map!(Db.rowToStruct!DbTestResult).map!(r => RestTestResult(
            name,
            getFromDb!DbProjectVersion(r.projectVersionId).name,
            getFromDb!DbArchitecture(r.architectureId).name,
            getFromDb!DbPlatform(r.platformId).name,
            RestCompiler(getFromDb!DbCompiler(r.compilerId).name, getFromDb!DbCompiler(r.compilerId).version_),
            (r.successFlags & SuccessFlags.Fetch) > 0,
            (r.successFlags & SuccessFlags.Build) > 0,
            (r.successFlags & SuccessFlags.Test) > 0,
            (includeLog) ? r.fetchLog : "",
            (includeLog) ? r.buildLog : "",
            (includeLog) ? r.testLog  : ""
        )).array;
    }
}