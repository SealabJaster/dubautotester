module tester.dub;

private
{
    import vibe.d;
    import tester.db;
    import core.time;
}

final static class Dub
{
    static const TIME_BETWEEN_FETCHES       = 4.hours;
    static const TIME_BETWEEN_INDEX_FETCHES = 1.days;
    static const TIME_BETWEEN_INFO_FETCHES  = 4.hours;

    public static
    {
        void onInit()
        {
            runTask(() => Dub.fetchPackages());
            runTask(() => fetchTask());
        }

        Json getOrCacheIndex()
        {
            auto bookkeeping = Db.getStructById!DbBookkeeping(1);
            auto newBookkeeping = bookkeeping;
            newBookkeeping.dateIndexLastFetched = Clock.currTime;

            if(newBookkeeping.dateIndexLastFetched - bookkeeping.dateIndexLastFetched > TIME_BETWEEN_INDEX_FETCHES)
            {
                logInfo("[BEGIN] Caching new version of index.json");
                
                newBookkeeping.codeDlangIndex = downloadJson("https://code.dlang.org/packages/index.json").toString();
                //newBookkeeping.codeDlangIndex = readFileUTF8("index.json");
                Db.insertOrUpdate(newBookkeeping);

                logInfo("[END] Caching new version of index.json");
            }

            return parseJsonString(newBookkeeping.codeDlangIndex);
        }

        void fetchPackages()
        {
            import std.algorithm : map;
            import std.range     : retro, take;
            import std.format    : format;
            logInfo("[BEGIN] Fetching packages");
            scope(exit) logInfo("[END] Fetching packages");

            const TEMP__limit = 21;
            int TEMP__count = 0;

            auto index     = Dub.getOrCacheIndex();
            auto compilers = Db.getAllEager!DbCompiler();
            auto archs     = Db.getAllEager!DbArchitecture();
            Db.doTransaction({
                foreach(projectName; index.get!(Json[]).map!(j => j.get!string))
                {
                    TEMP__count++;
                    if(TEMP__count >= TEMP__limit)
                        break;

                    logInfo("[Fetch] Processing: %s", projectName);

                    // Fetch and parse the information about the project.
                    Json projectJson;
                    Json versionsJson;
                    if(!Db.doesExistByName!DbProjectInfo(projectName))
                    {
                        auto projectInfoDb = DbProjectInfo(
                            -1, 
                            projectName, 
                            downloadJson("https://code.dlang.org/packages/%s.json".format(projectName)).toString(), 
                            Clock.currTime
                        );

                        projectJson = projectInfoDb.getInfo();                        
                        Db.insert(projectInfoDb);
                    }
                    else
                    {
                        auto projectInfoDb = Db.getStructByName!DbProjectInfo(projectName);
                        if(Clock.currTime - projectInfoDb.dateFetched > TIME_BETWEEN_INFO_FETCHES)
                        {
                            projectInfoDb.json = downloadJson("https://code.dlang.org/packages/%s.json".format(projectName)).toString();
                            projectInfoDb.dateFetched = Clock.currTime;
                        }

                        projectJson = projectInfoDb.getInfo();
                        Db.insertOrUpdate(projectInfoDb);
                    }

                    versionsJson = projectJson["versions"];

                    // Get/create the project.
                    DbProject projectDb;
                    if(!Db.doesExistByName!DbProject(projectName))
                    {
                        auto repoJson = projectJson["repository"];
                        projectDb = DbProject(-1, projectName, repoJson["kind"].get!string, repoJson["owner"].get!string, repoJson["project"].get!string);
                        Db.insert(projectDb); // Db.insert will update the id field to be the ID given from the insert.
                    }
                    else
                        projectDb = Db.getStructByName!DbProject(projectName);

                    // Get the versions we want.
                    foreach(ver; versionsJson.get!(Json[]).retro.take(3)) // Take only the latest 3 versions for now.
                    {
                        auto versionString = ver["version"].get!string;

                        // Branches are deprecated from what I remember, so we just skip them.
                        if(versionString[0] == '~')
                            continue;

                        // Get/create the version.
                        DbProjectVersion projectVersionDb;
                        if(Db.doesExistByName!DbProjectVersion(versionString))
                            projectVersionDb = Db.getStructByName!DbProjectVersion(versionString);
                        else
                        {
                            projectVersionDb.name      = versionString;
                            projectVersionDb.projectId = projectDb.id;
                            projectVersionDb.dateTime  = SysTime.fromISOExtString(ver["date"].get!string);
                            Db.insert(projectVersionDb);
                        }

                        // Check to see how many tests have to be schedueled.
                        foreach(arch; archs)
                        foreach(compiler; compilers)
                        {
                            if(Db.doesTestResultExist(projectDb, projectVersionDb, arch, compiler))
                                continue;

                            import tester.runner;
                            TestRunner.addToSchedule(ScheduledTest(projectDb, projectVersionDb, arch, compiler));
                        }
                    }
                }
            });
        }
    }

    private static
    {
        void fetchTask()
        {
            while(true)
            {
                auto bookkeep = Db.getStructById!DbBookkeeping(1);
                auto newBookkeep = bookkeep;

                newBookkeep.dateLastFetched = Clock.currTime;
                if(newBookkeep.dateLastFetched - bookkeep.dateLastFetched > TIME_BETWEEN_FETCHES)
                {
                    Db.insertOrUpdate(newBookkeep);
                    Dub.fetchPackages();
                }
                vibe.core.core.yield();
            }
        }
    }
}

Json downloadJson(string URL)
{
    string data;
    download(URL, (scope InputStream stream) { data = stream.readAllUTF8(); });

    return parseJsonString(data);
}