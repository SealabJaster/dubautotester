import vibe.d;
import vibe.core.core : vibeYield = yield;

import std.array : appender;
import std.file  : exists, mkdirRecurse, fileWrite = write;
import std.path  : buildPath;

import tester.platforms.core;
import tester.runner;
import tester.db;
import tester.dub;
import tester.rest.v1;

const REST_JS_FOLDER = "public/api/";

void main(string[] args)
{
    // Setup web server stuff.
    auto settings = new HTTPServerSettings;
    settings.port = 8080;

    auto router = new URLRouter();
    router.get("*", serveStaticFiles("public/"));
    router.registerRestInterface(new RestInterfaceV1());

    if(!REST_JS_FOLDER.exists)
        mkdirRecurse(REST_JS_FOLDER);

    auto buffer = appender!(char[]);
    generateRestJSClient!IRestInterfaceV1(buffer);
    fileWrite(buildPath(REST_JS_FOLDER, "v1.js"), buffer.data);

    // Generic stuff.
    HTTPClient.setUserAgentString = "vibe.d/"~vibeVersionString~" (Dub Auto Testing server) (HTTPClient, +http://vibed.org/)";
    setLogLevel(LogLevel.info);
    setupWorkerThreads();

    // Subsystems
    Db.onInit("test.db");
    TestRunner.onInit(getPlatform(), "test");
    Dub.onInit();

    // Vibe.d
    listenHTTP(settings, router);
    runApplication(&args);
}